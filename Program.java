package PatternProgram;

public class Program {

	public static void main(String[] args) {
		
		Program pro = new Program();
		pro.program1();
		pro.program2();
		pro.program3();
		pro.program4();

	}

	private void program4() {
		int rows = 5;
		for(int i=1;i<=rows;i++) {
			for(int j=rows;j>i;j--) {
				System.out.print(" ");
				}
			for(int k=1;k<=i;k++) {
				System.out.print(i+" ");
			}
             System.out.println();
			}		
	}

	private void program3() {
		for(int i=1;i<=5;i++) {
			for(int j=1;j<=5;j++) {
				System.out.print(i+" ");
				
				}
			System.out.println();

			}	
	}

	private void program2() {
		for(int i=1;i<=5;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(j+" ");
				}
			System.out.println();

			}	
	}

	private void program1() {
		for(int i=1;i<=5;i++) {
			for(int j=1;j<=i;j++) {
				System.out.print(i+" ");
				}
			System.out.println();

			}	
	}

}
